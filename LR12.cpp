﻿#include <iostream>
#include "windows.h"
#include <cstring>
#include <string> 
#include <sstream>


int strcmp(const char* string1, const char* string2);

using namespace std;

struct book
{
	char title[50];
	int number_of_copies;
	float price;
};
	
void show(const book obj[], int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << " Ім'я: " << obj[i].title << endl << " Кількість копій:" << obj[i].number_of_copies << endl << " Ціна: " << obj[i].price << endl << endl;
	}
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	const int n = 12;
	struct book shop[12] = {
		"Кобзар", 50, 299.99 ,
		"Я(Романтика)", 34, 250.99,
		"Лісова_пісня", 65, 141.99,
		"Місто", 78, 249.50,
		"Чорна_рада", 34, 199.99,
		"Мина_Мазайло", 55, 699.99,
		"Енеїда", 7, 129.99,
		"Зачарована_Десна", 56, 1299.99,
		"Інститутка", 14, 99.99,
		"Хіба_ревуть_воли,_як_ясла_повні?", 34, 288.10,
		"Кайдашева_сім'я", 76, 250.99,
		"Майстер_корабля", 34, 140.99 };
	int x;
	float b, m;
	
	cout << "Функції:" << endl << "1 - Вивести всю інформацію про книги." << endl << "2 - Знайти книгу." << endl << "3 - Пошук за ціною." << endl;
	cout << "Введіть номер функції:";
	cin >> x;
	cout << endl;
	if (x == 1)
	{
		show(shop, n);
	}
	if (x == 2)
	{
		cout << "Введіть назву книги(українською):";
		char key[50];
		cin  >> key;
		for (int i = 0; i < n; i++)
		{
			if (strcmp(shop[i].title, key) == 0)
			{
				cout << " Ім'я: " << shop[i].title << endl << " Кількість копій:" << shop[i].number_of_copies << endl << " Ціна: " << shop[i].price << endl << endl;
			}
		}
	}
	if (x == 3)
	{
		cout << "Введіть інтервал ціни/"<< endl << "Нижній показчик:";
		cin >> b;
		cout << "Верхній показчик:";
		cin >> m;
		for (int i = 0; i < n; i++)
		{
			if (shop[i].price > b && shop[i].price < m)
			{
				cout << " Ім'я: " << shop[i].title << endl << " Кількість копій:" << shop[i].number_of_copies << endl << " Ціна: " << shop[i].price << endl << endl;
			}
		}
	}
}





